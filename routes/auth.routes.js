const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/auth.controller');
const passport = require('passport');
const { check } = require('express-validator/check');


// Routes

router.post('/register', [
  check('name').exists().withMessage('Merci de fournir un nom complet').isLength({min: 3}).withMessage('Le nom complet doit contenir au moins 3 caractères'),
  check('username').exists().withMessage('Le nom d\'utilisateur  est obligatoire'),
  check('email').exists('L\'adresse email est obligatoire').isEmail().withMessage('Merci de fournir un adresse email valide'),
  check('password').isLength({min: 5}).withMessage('Le mot de passe doit contenir au moins 5 caractères')
  ], AuthController.register);

router.post('/login', [
  check('username').exists().withMessage('Aucun nom d\'utilisateur trouvé'),
  check('password').exists().withMessage('Aucun mot de passe trouvé')
], AuthController.login);

router.get('/auth/profile', passport.authenticate('jwt', {session: false}), AuthController.profile);

router.get('/confirm/:token', AuthController.confirm);

module.exports = router;
