const express = require('express');
const router = express.Router();
const TrackController = require('../controllers/track.controller');
const passport = require('passport');
const {checkSchema, check} = require('express-validator/check');
const Category = require('../models/Category.model');
const CategoryService = require('../services/category.service');

router.post('/auth/track', checkSchema({
    title: {
        isString: true,
        isOptional: false,
        errorMessage: 'Le titre du morceau est obligatoire',
        isLength: {
            errorMessage: 'Le titre de contenir au moins 2 caracteres',
            options: {
                min: 2
            }
        }
    },
    url: {
        isURL: true,
        isOptional: false,
        errorMessage: 'L\'URL du morceau est obligatoire'
    },
    category: {
        isOptional: false
    }
}), [
    check('category').exists().withMessage('La categorie du morceau est obligatoire')
                     .isMongoId().withMessage('Merci de fournir une categorie valide')
], passport.authenticate('jwt', { session: false }), TrackController.addTrack);


router.get('/auth/track/search/:query', passport.authenticate('jwt', {session: false}), TrackController.searchTrack);

// router.get('/init', (req, res, next) => {
//     let categories = [
//         'Pop',
//         'Rap',
//         'Électronique',
//         "R'n'B",
//         'Rock',
//         'Jazz',
//         'Latino',
//         'Arab',
//         'Metal',
//         'Country',
//         'Reggae',
//         'Soul & Funk',
//         'Classique'
//     ];

//     categories.forEach(item => {
//         const category = new Category({
//             name: item
//         });
//         CategoryService.createCategory(category);
//     });

//     res.json({
//         success: true,
//         message: 'Done'
//     })
// });

module.exports = router;