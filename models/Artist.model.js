const mongoose = require('mongoose');
const TrackSchema = require('./Track.model').schema;
const Schema = mongoose.Schema;

const ArtistSchema = mongoose.Schema({
    name: { type: String, required: true },
    tracks: [{ type: Schema.Types.ObjectId, ref: 'Track' }]
});

const Artist = module.exports = mongoose.model('Artist', ArtistSchema);