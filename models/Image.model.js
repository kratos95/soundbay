const mongoose = require('mongoose');

const ImageSchema = mongoose.Schema({
    title: { type: String, required: false },
    url: { type: String, required: true }
});

const Image = module.exports = mongoose.model('Image', ImageSchema);