const mongoose = require('mongoose');
const TrackSchema = require('./Track.model').schema;
const PlaylistSchema = require('./Playlist.model').schema;

const UserSchema = mongoose.Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  username: { type: String, required: true },
  password: { type: String, required: true },
  enabled: { type: Boolean, default: false },
  created_at: { type: Date, default: Date.now },
  uploaded_tracks: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Track' }],
  playlists: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Playlist' }]
});

UserSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    delete ret.password;
    delete ret.enabled;
    delete ret.created_at;
    delete ret.__v;
    return ret;
  }
});

const User = module.exports = mongoose.model('User', UserSchema);
