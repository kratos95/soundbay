const mongoose = require('mongoose');
const ArtistSchema = require('./Artist.model').schema;
const Schema = mongoose.Schema;
const ImageSchema = require('./Image.model').schema;
const UserSchema = require('./User.model').schema;
const CategorySchema = require('./Category.model').schema;

const TrackSchema = mongoose.Schema({
    title: { type: String, required: true },
    url: { type: String, required: true },
    poster: { type: Schema.Types.ObjectId, ref: 'Image' },
    category: { type: Schema.Types.ObjectId, ref: 'Category', required: true },
    artist: { type: Schema.Types.ObjectId, ref: 'Artist' },
    uploader: { type: Schema.Types.ObjectId, ref: 'User' }
});

TrackSchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.__v;
        return ret;
    }
});

const Track = module.exports = mongoose.model('Track', TrackSchema);