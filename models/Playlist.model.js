const mongoose = require('mongoose');
const ImageShema = require('./Image.model').schema;
const UserSchema = require('./User.model').schema;
const TrackSchema = require('./Track.model').schema;

const PlaylistSchema = mongoose.Schema({
    title: { type: String, required: true },
    private: { type: Boolean, default: false },
    poster: { type: mongoose.Schema.Types.ObjectId, ref: 'Image' },
    tracks: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Track' } ],
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
});

const Playlist = module.exports = mongoose.model('Playlist', PlaylistSchema);