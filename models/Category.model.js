const mongoose = require('mongoose');
const ImageSchema = require('./Image.model').schema;

const CategorySchema = mongoose.Schema({
    name: { type: String, required: true },
    image: { type: mongoose.Schema.Types.ObjectId, ref: 'Image' }
});

CategorySchema.set('toJSON', {
    transform: (doc, ret, options) => {
        delete ret.__v;
        return ret;
    }
});

const Category = module.exports = mongoose.model('Category', CategorySchema);