const User = require('../models/User.model');
const UserService = require('../services/user.service');
const jwt  = require('jsonwebtoken');
const passport = require('passport');
const ExtractJwt = require('passport-jwt').ExtractJwt;

const sendgrid = require('@sendgrid/mail');
sendgrid.setApiKey(process.env.SENDGRID_API_KEY);

const {validationResult} = require('express-validator/check');

module.exports.register = (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  let user = new User({
    name: req.body.name,
    email: req.body.email,
    username: req.body.username,
    password: req.body.password
  });


  UserService.checkIfExists({$or: [{username: user.username}, {email: user.email}]}, (err, users) => {
    if (err) throw err;

    if (users.length > 0) {
      res.status(409).json({
        success: false,
        message: 'L\'adresse email ou nom d\'utilisateur existe deja',
      });
    } else {
      UserService.createUser(user, (err, createdUser) => {
        if (err) {
          return res.status(500).json({success: false, message: 'Erreur lors de l\'inscription'});
        }
        else {
          const token = jwt.sign({user: createdUser}, process.env.SECRET, {
            expiresIn: 3600 * 48
          });

          const message = {
            to: createdUser.email,
            from: 'noreply@soundbay.tk',
            subject: 'Confirmation SoundBay',
            text: `Confirmez votre email en appuyant sur ${process.env.DOMAIN}/api/confirm/${token}`
          };
          sendgrid.send(message);

          res.status(201).json({success: true, message: 'Inscription réussite, merci de confirmer votre adresse email'});
        }
      });
    }
  });
}


module.exports.login = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  const username = req.body.username;
  const password = req.body.password

  UserService.getUserByUsername(username, (err, user) => {
    if (err) throw err;
    
    if (!user)
      return res.status(404).json({success: false, message: 'Message non trouvé'});

    UserService.comparePassword(password, user.password, (err, isMatch) => {
      if (err) throw err;
      if (isMatch) {
        const token = jwt.sign({user: user}, process.env.SECRET, {
          expiresIn: 3600
        });
        res.json({
          success: true,
          token: `JWT ${token}`,
          user: {
            id: user._id,
            name: user.name,
            username: user.username,
            email: user.email,
            created_at: user.created_at
          }
        });

      } else {
        return res.status(400).json({success: false, message: 'Identifiants invalides'});
      }

    });
  });
}

module.exports.profile = (req, res, next) => {
  res.json(req.user);
}

module.exports.confirm = (req, res, next) => {

  jwt.verify(req.params.token, process.env.SECRET, (err, decoded) => {
    if (err) throw err;
    UserService.getUserById(decoded.user._id, (err, persistedUser) => {
      if (err)
        throw err;
      persistedUser.enabled = true;
      UserService.updateUser(persistedUser, (err, dbUser) => {
        if (err)
          res.statusStatus(400);
        else
          res.redirect(`${process.env.DOMAIN}/`);
      });
    });
  });
}
