const User = require('../models/User.model');
const Track = require('../models/Track.model');
const TrackService = require('../services/track.service');

const {validationResult} = require('express-validator/check');

module.exports.addTrack = async (req, res, next) => {
    try {
        const errors = await validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }

        let track = new Track({
            title: req.body.title,
            url: req.body.url,
            artist: req.body.artist,
            uploader: req.user._id,
            category: {
                _id: req.body.category
            }
        });

        await TrackService.createTrack(track);

        res.status(201).json({
            success: true,
            message: 'Le morceau a été crée',
            data: track
        });
    } catch (e) {
        res.status(500).json({
            success: false,
            message: e.message
        });
    }
}


module.exports.searchTrack = async (req, res, next) => {
    try {
        console.log('query', req.params.query)
        let results = await TrackService.searchTrack(req.params.query);
        res.json(results);
    } catch (e) {
        throw e;
    }
}