require('dotenv').config();

const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

const mongoose = require('mongoose');
const bluebird = require('bluebird');


mongoose.Promise = bluebird;

mongoose.connect(process.env.DB)
  .then(() => {
    console.log('Succesfully connected to MongoDB');
  })
  .catch(() => {
    console.log('Error connecting to the MongoDB');
  });

const app = express();
const port = 3000 || process.env.PORT;


app.use(cors());
app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());

require('./authorization/passport')(passport);

// Importing routes
const auth = require('./routes/auth.routes');
const track = require('./routes/track.routes');
app.use('/api/', auth);
app.use('/api/', track)


// Defining static directory
app.use(express.static(path.join(__dirname, 'client')));

app.get('/', (req, res) => {
  res.send('Hello World !');
});

app.listen(port, () => {
  console.log(`Server started on port ${port}` );
});
