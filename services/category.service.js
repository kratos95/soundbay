const Category = require('../models/Category.model');

module.exports.createCategory = async (category) => {
    try {
        return await category.save();
    } catch (e) {
        throw Error(e.mesage);
    }
}