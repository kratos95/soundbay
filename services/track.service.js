const Track = require('../models/Track.model');

module.exports.createTrack = (track) => {
    track.save();
}

module.exports.searchTrack = (q) => {
    return Track.find({ title: new RegExp(q, 'i') }).populate('uploader').populate('category');
}